import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) throws InterruptedException, IOException {

		final String[] pathList = new String[]
				{
						new String("plik.txt"),
						new String("plik.txt"),
						new String("plik.txt"),
						new String("plik.txt"),
						new String("plik.txt"),
						new String()
				};

		ParalleLogger paralleLogger = new ParalleLogger();
		Thread logThread = new Thread(paralleLogger);
		logThread.start();

		Monitor monitor = new Monitor(5, pathList);
		monitor.studentAddedEvent.add(( source, student ) -> paralleLogger.log( "ADD", student ) );
		monitor.studentRemovedEvent.add(( source, student ) -> paralleLogger.log( "REMOVE", student ) );
		monitor.run();
	}

}
