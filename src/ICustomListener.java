import java.util.EventListener;

@FunctionalInterface
public interface ICustomListener<T> extends EventListener
{
    void handle( Object source, T object );
}
