import java.util.Comparator;

/**
 * Created by Mateusz on 19.03.2017.
 */
public class Komparator implements Comparator<Student>
{

    @Override
    public int compare(Student o1, Student o2) {
        if (o2==null) return -1;
        else if ( o1.getMark() > o2.getMark() ) return 1;
        else if ( o1.getMark() < o2.getMark() ) return -1;
        else return 0;

    }
}
