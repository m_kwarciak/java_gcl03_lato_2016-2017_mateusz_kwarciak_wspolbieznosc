/**
 * Created by Mateusz on 23.04.2017.
 */
public class MonitorException extends Exception {
    private String message;

    public MonitorException(String message){
        this.message=message;
    }

    public void write()
    {
        System.out.println(message);
    }
}
