/**
 * Created by Mateusz on 20.03.2017.
 */
public class CrawlerException extends Exception {
    private String wiadomosc;

    CrawlerException(String wiadomosc)
    {
        this.wiadomosc=wiadomosc;
    }

   public void pisz()
    {
        System.out.print(wiadomosc);
    }

}
